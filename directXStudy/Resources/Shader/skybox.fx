#ifndef _SKYBOX_FX_
#define _SKYBOX_FX_

#include "params.fx"

struct VS_IN
{
    float3 localPos : POSITION;
    float2 uv : TEXCOORD;
};

struct VS_OUT
{
    float4 pos : SV_Position;
    float2 uv : TEXCOORD;
};

VS_OUT VS_Main(VS_IN input)
{
    VS_OUT output = (VS_OUT)0;

    // 스카이박스 로컬 포지션이 0 이기 때문에
    // 바로 뷰 스페이스로 옮기기 위해서 위치값을 제외한 로테이션만 적용해서
    // 뷰 스페이스로 변환.
    float4 viewPos = mul(float4(input.localPos, 0.0f), g_matView);

    //투영스페이스인데 이 과정에서 동차좌표계 개념이 포함되어 있어서 클립스페이스라고 적은듯..
    float4 clipSpacePos = mul(viewPos, g_matProjection);

    // w/w=1이기 때문에 항상 깊이가 1로 유지된다.
    output.pos = clipSpacePos.xyww;
    output.uv = input.uv;

    return output;
}

float4 PS_Main(VS_OUT input) : SV_Target
{
    float4 color = g_tex_0.Sample(g_sam_0, input.uv);
  

     return color;
}

#endif